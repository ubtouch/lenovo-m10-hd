![pipeline status](https://gitlab.com/uports/h10/lenovo-m10-hd/lenovo-m10-hd/badges/main/pipeline.svg)
# Lenovo Tab M10 (2nd Gen) LTE TB-X306NC and TB-X306V
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
______________________

![](./refs/m10hd.png)

Lenovo Tab M10 HD (amar_row_lte) specs
==========================================


| Basic                   | Spec Sheet                                                                                                                     |
| -----------------------:|:------------------------------------------------------------------------------------------------------------------------------ |
| CPU                     | Octa-core (4x2.3 GHz Cortex-A53 & 4x1.8 GHz Cortex-A53)                                                                                                                      |
| Chipset                 | Mediatek MT6762 Helio P22T (12 nm)                                                                                                            |
| GPU                     | PowerVR GE8320                                                                                                                   |
| Memory                  | 4 GB RAM                                                                                                                     |
| Shipped Android Version | Android 10                                                                                                                           |
| Storage                 |64GB                                                                                                                  |
| Battery                 | Li-Po 5000 mAh, non-removable battery                                                                                           |
| Display                 | 800 x 1280 pixels, 16:10 ratio (~149 ppi density)                                                                            |
| Camera (Back)(Main)     | 8 MP, AF                                                                                |
| Camera (Front)          | 5 MP
| Kernel Version          | 4.9.190 "Roaring Lionus"

# What works so far?

### Progress
![100%](https://progress-bar.dev/100) Ubuntu 20.04 Focal

- [X] Recovery
- [X] Boot
- [X] Bluetooth
- [X] Camera Fotos and Video Recording
- [x] GPS
- [X] Audio works
- [X] Bluetooth Audio
- [X] Waydroid
- [X] MTP
- [X] ADB
- [X] SSH
- [X] Online charge
- [X] Offline Charge
- [X] Wifi
- [X] SDCard
- [x] Wireless display
- [X] Manual Brightness Works
- [X] Hardware video playback
- [X] Rotation
- [X] Proximity sensor
- [X] Virtualization
- [X] GPU
- [X] Lightsensor
- [X] Proximity sensor
- [X] Automatic brightness
- [X] Hotspot
- [X] Airplane Mode
- [X] SIM Card

## Caution
### in order to install Ubuntu Touch on device, Android 11 firmware is required please download the following package:
- Android [11 Firmware](https://mirrors.lolinet.com/firmware/lenowow/Tab_M10_HD_2nd_Gen/TB-X306V/) .
- Extract the Firmware.tar.xz
- Download [SP Flash Tool](https://spflashtool.com/) for your system.
- Open the SP Flash Tool and you should call the files inside the firmware directory like the picture below and always select Firmware Update on tab:

![spflashtool](./refs/spflashtool.png)

- MTK_AllInOne_DA_6765.bin
- MT6765_Android_scatter.txt
- auth_sv5.auth

## Unlock Bootloader
- Put the device in fastbot by holding VOLUME DOWN and POWER keys together
- Connect to Computer and run the following:

```command
fastboot flashing unlock
```
- Now confirm on device by pressing the VOLUME UP key.
  
## Install:

- Download The latest devel-flashable-focal image from latest CI builds
https://gitlab.com/ubtouch/lenovo-m10-hd/-/pipelines

### Put the device in fastboot mode (bootloader)
Hold VOLUME DOWN + POWER BUTTON

- Flash the Ubuntu boot.img:
```command
fastboot flash boot ./boot.img
```
- Download [recovery.img](https://github.com/rubencarneiro/amar_row_wifi/releases/download/1.0/recovery.img)

- Flash Ubports recovery.img:
```command
fastboot flash recovery ./recovery.img
```
- Download [vbmeta.img](https://github.com/rubencarneiro/amar_row_wifi/releases/download/1.0/vbmeta.img)

```command
fastboot --disable-verity --disable-verification flash vbmeta ./vbmeta.img
```
- Optinal flash custom logo
```command
fastboot flash logo ./logo.bin
```
- Format userdata you may require fastboot binary version 30.0.0-6374843
```command
fastboot format:ext4 userdata
```
- Reboot to fastbootd
```command
fastboot reboot fastboot
```
- Delete product logical partition
```command
fastboot delete-logical-partition product
```
- Resize system partition
```command
fastboot resize-logical-partition system 3978565472
```
- Flash Ubuntu Rootfs:
```command
fastboot flash system ./ubuntu.img
```


__________________________________

## Im not endorsed or founded by UBports Foundation and i do this for fun but does take time work and resources, so if you want to support and apreciate my work pleaso do Donate:
___________________________________
<center>
<a href="https://paypal.me/rubencarneiro?locale.x=pt_PT" title="PayPal" onclick="javascript:window.open('https://paypal.me/rubencarneiro?locale.x=pt_PT','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/mktg/Logo/pp-logo-150px.png" border="0" alt="PayPal Logo"></a>
